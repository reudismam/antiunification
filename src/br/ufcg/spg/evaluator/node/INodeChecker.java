package br.ufcg.spg.evaluator.node;

public interface INodeChecker {
  public boolean check();
}
