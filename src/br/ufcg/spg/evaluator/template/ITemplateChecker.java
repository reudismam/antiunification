package br.ufcg.spg.evaluator.template;

public interface ITemplateChecker {
  public boolean check();
}
